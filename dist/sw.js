self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open('default-cache').then(function(cache) {
            return cache.addAll([
                '/',
                '/index.html',
                '/bundle.js',
                '/libraries/p5.dom.min.js',
                '/libraries/p5.min.js',
                '/libraries/p5.sound.min.js',
                './assets/icon.png',
                './assets/8ball.png',
                './assets/8ballSounds/asISeeItYes.mp3',
                './assets/8ballSounds/askAgainLater.mp3',
                './assets/8ballSounds/betterNotTellYouNow.mp3',
                './assets/8ballSounds/cannotPredictNow.mp3',
                './assets/8ballSounds/concentrateAndAskAgain.mp3',
                './assets/8ballSounds/doNotCountOnIt.mp3',
                './assets/8ballSounds/itIsCertain.mp3',
                './assets/8ballSounds/ItisDecidedlySo.mp3',
                './assets/8ballSounds/mostLikely.mp3',
                './assets/8ballSounds/myReplyIsNo.mp3',
                './assets/8ballSounds/mySourcesSayNo.mp3',
                './assets/8ballSounds/outlookGood.mp3',
                './assets/8ballSounds/outlookNotSoGood.mp3',
                './assets/8ballSounds/replyHazyTryAgain.mp3',
                './assets/8ballSounds/signsPointToYes.mp3',
                './assets/8ballSounds/veryDoubtful.mp3',
                './assets/8ballSounds/withoutADoubt.mp3',
                './assets/8ballSounds/yesDefinitely.mp3',
                './assets/8ballSounds/yes.mp3',
                './assets/8ballSounds/youMayRelyOnIt.mp3',
                '/manifest.json',
            ]);
        }).then(function() {
            return self.skipWaiting();
        }));
});

self.addEventListener('activate', function(e) {
    e.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function(e) {
    e.respondWith(
        caches.match(e.request).then(function(response) {
            return response || fetch(e.request)
        })
    )
});
