import { animals } from './arrayAnimalsObject';

export const animalSketch = (p) => {
    let sounds = [], images = [], image, sound, soundMessage = '', shake = false;

    p.preload = () => {
        for (let i = 0; i < animals.length; i++) {
            let animalName = animals[i]['name'],
                img = p.loadImage('assets/animalPictures/' + animalName + '.jpg');
            img.i = i;    
            images.push(img);
            let soundFile = p.loadSound('assets/animalSounds/' + animalName + '.mp3');
            soundFile.subtitle = 'The ' + animalName + 'says';
            soundFile.i = i;
            sounds.push(soundFile);
        }
    }

    p.setup = () => {
        image = getRandomImage();
        p.createCanvas(600, 500);
        p.setShakeThreshold(50);
        p.textSize(30);
        p.imageMode(p.CENTER);
        p.textAlign(p.CENTER);
    }
    p.draw = () => {
        p.image(image, p.width/2, p.height/2, p.width, p.height);
        console.log(image);
    }
    p.mousePressed = () => {
        shake = true;
        p.deviceShaken();
    }
    p.deviceShaken = () => {
        if (shake) {
            shake = false;
            sound = getRandomSound();
            soundMessage = sound.subtitle;
            sound.play();
            sound.onended(function() {
                soundMessage = '';
                shake = false;
            });
            image = getRandomImage();
        }
    }
    function getRandomSound() {
        let i = Math.floor(Math.random() * sounds.length);
        return sounds[i];
    }
    function getRandomImage() {
        let i = Math.floor(Math.random() * sounds.length);
        return images[i];
    }
}
