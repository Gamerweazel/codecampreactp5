import { mappedSounds } from './mapped8BallSounds';

export const ballSketch = (p) => {

let mic, recorder, soundFile, sounds = [], micVol, shake = false, timer = 120,
  response, responseMessage = '', amp, message, img;

 p.preload = () => {
  img = p.loadImage('assets/8ball.png');
  for (let i = 0; i < 20; i++) {
    let file = p.loadSound(mappedSounds[i]['fileName']);
      file.subtitle = mappedSounds[i]['subtitle'];
    sounds.push(file);
  }
}

p.setup = () => {
    p.createCanvas(p.windowWidth, p.windowHeight);
    p.setShakeThreshold(50);
    p.textSize(30);
    p.imageMode(p.CENTER);
    p.textAlign(p.CENTER);
    mic = new p5.AudioIn();
    mic.start();
    amp = new p5.Amplitude();
}

p.draw = () => {
  p.background(100);
  amp.getLevel();
  micVol = mic.getLevel();
  if (micVol > 0.2) {
    shake = true;
    timer = 180;
  } else
      timer--;
      if (timer < 0) {
        shake = false;
      }
  let a = amp.getLevel(),
    resRadius = p.map(a, 0, 1, 300, 700);
  if (!shake)
      message = 'I am listening...';
  else {
      message = 'shake';
      p.background('green');
  }
  if (p.mouseIsPressed && shake)
      p.image(img, p.mouseX, p.mouseY, resRadius, resRadius);
  else
      p.image(img, p.windowWidth/2, p.windowHeight/2, resRadius, resRadius);
  p.fill(255);
  p.text(message, p.windowWidth/2, p.windowHeight*.8);
  if (responseMessage)
    p.text(responseMessage, p.windowWidth/2, p.windowHeight*.9);
}

p.mouseReleased = () => {
  if (p.pmouseX == p.mouseX && p.pmouseY == p.mouseY || !shake) return
  else {
      p.deviceShaken();
  }
}

p.deviceShaken = () => {
  if (shake) {
    shake = false;
    response = getRandomSound();
    responseMessage = response.subtitle;
    amp.setInput(response);
    response.play();
    response.onended(function() {
      responseMessage = '';
      shake = false;
    })
  }
}

function getRandomSound() {
  let i = Math.floor(Math.random()*19 + 1);
    return sounds[i];
}

p.keyPressed = () => {
  if (p.key === ' ') {
    if (state === 0 && mic.enabled) {
        recorder.record(soundFile);
        p.background(255,0,0);
        p.text('Recording!', 20, 20);
        state++;
  }
    else if (state === 1) {
        p.background(0,255,0);
        recorder.stop();
        fft.setInput(soundFile);
        soundFile.play();
        state++;
    }
  }
}
}
