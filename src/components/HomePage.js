import React, { Component } from 'react'
import P5Component from './P5Component'
import { ballSketch } from '../sketches/ballSketch'
import { animalSketch } from '../sketches/animalSketch'

export default class HomePage extends Component {
    constructor(props) {
        super(props)
        this.changeSketch = this.changeSketch.bind(this)
        this.state = {
            selectedSketch: ballSketch
        }
    }

    changeSketch() {
        let sketch = this.state.selectedSketch == ballSketch ? animalSketch : ballSketch;
        this.setState({selectedSketch: sketch});
    }

    render() {
        return(
            <div>
                <P5Component sketch={this.state.selectedSketch} />
            </div>
        )
    }
}
