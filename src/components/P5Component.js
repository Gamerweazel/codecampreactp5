import React, { Component } from 'react'

export default class P5Component extends Component {
    constructor(props) {
        super(props)
        this.state = {
          sketch: this.props.sketch
        }
    }

    renderNewSketch() {
        this.sketch = new p5(this.state.sketch, this.refs.sketch);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.state) {
            this.setState({sketch: nextProps.sketch}, this.renderNewSketch);
            this.sketch.remove();
        }
    }

    componentDidMount() {
        this.sketch = new p5(this.state.sketch, this.refs.sketch);
    }

    render(){
        return(
            <div className='row center-lg center-md center-sm center-xs'>
                <div id='sketch' className='col' ref='sketch' />
            </div>
        )
    }
}
