import React, { Component } from 'react';
import { render } from 'react-dom';
import { Router, IndexRoute, browserHistory } from 'react-router';
import { routes } from './routes'

render(
    <Router history={browserHistory}>
        {routes}
    </Router>
    , document.getElementById('app')
)
